#ifndef PS2_RENDERER_PS2SDK_H_INCLUDED
#define PS2_RENDERER_PS2SDK_H_INCLUDED

#include <malloc.h>
#include <math3d.h>
#include <kernel.h>
#include <stdlib.h>
#include <tamtypes.h>
#include <packet.h>
#include <dma_tags.h>
#include <gif_tags.h>
#include <gs_psm.h>
#include <dma.h>
#include <graph.h>
#include <draw.h>
#include <draw3d.h>

#include <VertexBuffer.h>
#include <Texture.h>
#include <Math.h>

namespace PS2
{

class Renderer
{
public:
    Renderer();
    ~Renderer();

    bool Init();

    void Update();

    void DrawVertexBuffer(
        Mat4& modelMat,
        Mat4& viewMat,
        VertexBuffer& vertexBuffer
    )
    {
        if (vertexBuffer.mIndicesCount == 0)
        {
            q = render_mesh_unindexed(
                modelMat,
                viewMat,
                (VECTOR*)vertexBuffer.mVertices,
                (VECTOR*)vertexBuffer.mNormals,
                (VECTOR*)vertexBuffer.mColors,
                (VECTOR*)vertexBuffer.mTexCoords,
                vertexBuffer.mVertexCount,
                q,
                &prim,
                &color,
                mFrame,
                &mZ
            );
        }
        else
        {
            q = render_mesh(
                modelMat,
                viewMat,
                (VECTOR*)vertexBuffer.mVertices,
                (VECTOR*)vertexBuffer.mNormals,
                (VECTOR*)vertexBuffer.mColors,
                (VECTOR*)vertexBuffer.mTexCoords,
                vertexBuffer.mVertexCount,
                vertexBuffer.mIndices,
                vertexBuffer.mIndicesCount,
                q,
                &prim,
                &color,
                mFrame,
                &mZ
            );
        }
    }

    void SetTexture(Texture& tex) {
        if (mCurTex != &tex) {
            flushPipeline();
            
            qword_t* localQ = Texture::sLoadTexPkt->data;
            mCurTex = &tex;
            tex.load_texture(
                tex.mData,
                tex.mNumChannels,
                tex.mWidth, tex.mHeight,
                &Texture::sTexbuf,
                /*&q*/&localQ
            );
            // TODO - is this necessary?
            localQ = Texture::sSetupTexPkt->data;
            tex.setup_texture(
                tex.mWidth, tex.mHeight,
                &Texture::sTexbuf,
                /*&q*/&localQ
            );
        }
    }

private:

    framebuffer_t mFrame[2];
    zbuffer_t mZ;

    Mat4 mProjMat;

    // packetes for doublebuffering dma sends
    packet_t* packets[2];
    packet_t* current;
    int context = 0;

    // This packet is special for framebuffer switching
    packet_t* flip_pkt;

    qword_t* q;
    qword_t* dmatag;
    u64* dw;
    
    prim_t prim;
    color_t color;

    xyz_t* xyz;
    color_t* rgbaq;
    texel_t* st;

    static const size_t sMaxVertCount = 5000;
    VECTOR* temp_normals;
    VECTOR* temp_lights;
    VECTOR* temp_colours;   
    VECTOR* temp_vertices;

    int light_count = 4;
    VECTOR light_direction[4] = {
        {0.00f, 0.00f, 0.00f, 1.00f},
        {1.00f, 0.00f, -1.00f, 1.00f},
        {0.00f, 1.00f, -1.00f, 1.00f},
        {-1.00f, -1.00f, -1.00f, 1.00f}
    };
    VECTOR light_colour[4] = {
        {0.00f, 0.00f, 0.00f, 1.00f},
        {1.00f, 0.00f, 0.00f, 1.00f},
        {0.30f, 0.30f, 0.30f, 1.00f},
        {0.50f, 0.50f, 0.50f, 1.00f}
    };
    int light_type[4] = {
        LIGHT_AMBIENT,
        LIGHT_DIRECTIONAL,
        LIGHT_DIRECTIONAL,
        LIGHT_DIRECTIONAL
    };

    Texture* mCurTex;

    void init_gs(framebuffer_t* frame, zbuffer_t* z);
    void init_drawing_environment(framebuffer_t* frame, zbuffer_t* z);
    void flip_buffers(packet_t* flip, framebuffer_t* frame);
    void BeginFrame();

    qword_t* render_mesh(
        Mat4& modelMat,
        Mat4& viewMat,
        VECTOR* vertices,
        VECTOR* normals,
        VECTOR* colours, 
        VECTOR* coordinates, // tex coords
        int vertex_count,
        int* points,
        int points_count,
        qword_t* q,
        prim_t* prim,
        color_t* color,
        framebuffer_t* frame,
        zbuffer_t* z
    );
    qword_t* render_mesh_unindexed(
        Mat4& modelMat,
        Mat4& viewMat,
        VECTOR* vertices,
        VECTOR* normals,
        VECTOR* colours,
        VECTOR* coordinates, // tex coords
        int vertex_count,
        qword_t* q,
        prim_t* prim,
        color_t* color,
        framebuffer_t* frame,
        zbuffer_t* z
    );

    void flushPipeline()
    {
        dmatag = q;
        q++;
        q = draw_finish(q);
        DMATAG_END(dmatag, q-dmatag-1, 0,0,0);

        // now send the current dma chain
        //dma_wait_fast();
        dma_channel_send_chain(DMA_CHANNEL_GIF, current->data, q - current->data, 0,0);
        dma_wait_fast();

        // Begin frame without screen clear
        current = packets[context];
        q = current->data;
    }
};

} // end namespace PS2

#endif // PS2_RENDERER_PS2SDK_H_INCLUDED

