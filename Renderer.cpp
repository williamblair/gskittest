#include <Renderer.h>
#include <sifrpc.h>
#include <sbv_patches.h>
#include <loadfile.h>
#include <stdio.h>
#include <string.h>

namespace PS2
{

Renderer::Renderer() :
    mCurTex(nullptr)
{
}

Renderer::~Renderer()
{
    free(packets[0]);
    free(packets[1]);
}

bool Renderer::Init()
{
    // cdfs support
    {

        printf("SifInitRpc(0)\n");
        SifInitRpc(0);
        if (strcmp(ASSETS_DIR, "cdfs:") == 0) {
            #include "cdfs.h"
            // enable load modules from EE ram
            printf("Patch enable lmb\n");
            int ret = sbv_patch_enable_lmb();
            if (ret != 0) {
                printf("Load module patch failed\n");
                return false;
            }

            int irxRet = 0;
            ret = SifExecModuleBuffer(
                cdfs_irx,
                size_cdfs_irx,
                0,
                nullptr,
                &irxRet
            );
            printf("  cdfs ret, irx ret: %d, %d\n", ret, irxRet);
        }
    }

    // Init GIF dma channel
    dma_channel_initialize(DMA_CHANNEL_GIF, NULL, 0);
    dma_channel_fast_waits(DMA_CHANNEL_GIF);

    // Init the GS, framebuffer, and zbuffer
    init_gs(mFrame, &mZ);

    // Init the drawing environment and framebuffer
    init_drawing_environment(mFrame, &mZ);

    packets[0] = packet_init(40000, PACKET_NORMAL);
    packets[1] = packet_init(40000, PACKET_NORMAL);

    // Uncached accelerated
    flip_pkt = packet_init(3, PACKET_UCAB);

// untextured
#if 0
    prim.type = PRIM_TRIANGLE;
    prim.shading = PRIM_SHADE_GOURAUD;
    prim.mapping = DRAW_DISABLE;
    prim.fogging = DRAW_DISABLE;
    prim.blending = DRAW_ENABLE;
    prim.antialiasing = DRAW_DISABLE;
    prim.mapping_type = DRAW_DISABLE;
    prim.colorfix = PRIM_UNFIXED;
#endif
// Textured
    prim.type = PRIM_TRIANGLE;
    prim.shading = PRIM_SHADE_GOURAUD;
    prim.mapping = DRAW_ENABLE;
    prim.fogging = DRAW_DISABLE;
    prim.blending = DRAW_ENABLE;
    prim.antialiasing = DRAW_DISABLE;
    prim.mapping_type = PRIM_MAP_ST;
    prim.colorfix = PRIM_UNFIXED;

    color.r = 0x80;
    color.g = 0x80;
    color.b = 0x80;
    color.a = 0x80/*0x40*/;
    color.q = 1.0f;

    // Allocate calculation space
    //const size_t vertex_count = 4000; // TODO - configure/set max vertex count
    temp_normals = (VECTOR*)memalign(128, sizeof(VECTOR) * sMaxVertCount);
    temp_lights = (VECTOR*)memalign(128, sizeof(VECTOR) * sMaxVertCount);
    temp_colours = (VECTOR*)memalign(128, sizeof(VECTOR) * sMaxVertCount);
    temp_vertices = (VECTOR*)memalign(128, sizeof(VECTOR) * sMaxVertCount);

    // Allocate register space
    xyz = (xyz_t*)memalign(128, sizeof(u64) * sMaxVertCount);
    rgbaq = (color_t*)memalign(128, sizeof(u64) * sMaxVertCount);
    st = (texel_t*)memalign(128, sizeof(u64) * sMaxVertCount);

    // create view_screen matrix
    mProjMat = Math::Perspective(
        Math::Deg2Rad(45.0f),
        graph_aspect_ratio(),
        1.0f,
        2000.0f
    );

    // Prepare for first frame
    BeginFrame();

    return true;
}

void Renderer::init_gs(framebuffer_t* frame, zbuffer_t* z)
{

    // Define a 32-bit 640x512 framebuffer.
    frame->width = 640;
    frame->height = 512;
    frame->mask = 0;
    frame->psm = GS_PSM_32;

    // Allocate some vram for our framebuffer.
    frame->address = graph_vram_allocate(frame->width,frame->height, frame->psm, GRAPH_ALIGN_PAGE);

    frame++;

    frame->width = 640;
    frame->height = 512;
    frame->mask = 0;
    frame->psm = GS_PSM_32;

    // Allocate some vram for our framebuffer.
    frame->address = graph_vram_allocate(frame->width,frame->height, frame->psm, GRAPH_ALIGN_PAGE);

    // Enable the zbuffer.
    z->enable = DRAW_ENABLE;
    z->mask = 0;
    z->method = ZTEST_METHOD_GREATER_EQUAL;
    z->zsm = GS_ZBUF_32;
    z->address = graph_vram_allocate(frame->width,frame->height,z->zsm, GRAPH_ALIGN_PAGE);

    // Initialize the screen and tie the first framebuffer to the read circuits.
    graph_initialize(frame->address,frame->width,frame->height,frame->psm,0,0);
}

void Renderer::init_drawing_environment(framebuffer_t* frame, zbuffer_t* z)
{

    packet_t *packet = packet_init(20,PACKET_NORMAL);

    // This is our generic qword pointer.
    qword_t *q = packet->data;

    // This will setup a default drawing environment.
    q = draw_setup_environment(q,0,frame,z);

    // Now reset the primitive origin to 2048-width/2,2048-height/2.
    q = draw_primitive_xyoffset(q,0,(2048-320),(2048-256));

    // Finish setting up the environment.
    q = draw_finish(q);

    // Now send the packet, no need to wait since it's the first.
    dma_channel_send_normal(DMA_CHANNEL_GIF,packet->data,q - packet->data, 0, 0);
    dma_wait_fast();

    free(packet);
}

void Renderer::flip_buffers(packet_t* flip, framebuffer_t* frame)
{
    qword_t *q = flip->data;

    q = draw_framebuffer(q,0,frame);
    q = draw_finish(q);

    dma_wait_fast();
    dma_channel_send_normal_ucab(DMA_CHANNEL_GIF,flip->data,q - flip->data, 0);

    draw_wait_finish();
}

qword_t* Renderer::render_mesh(
    Mat4& modelMat,
    Mat4& viewMat,
    VECTOR* vertices,
    VECTOR* normals,
    VECTOR* colours,
    VECTOR* coordinates, // tex coords
    int vertex_count,
    int* points,
    int points_count,
    qword_t* q,
    prim_t* prim,
    color_t* color,
    framebuffer_t* frame,
    zbuffer_t* z)
{

    int i;

    qword_t *dmatag;

    MATRIX local_light;
    VECTOR light_rotation = { 0.00f, 0.00f, 0.00f, 1.00f };

    MATRIX local_screen;

    // Now grab our qword pointer and increment past the dmatag.
    dmatag = q;
    q++;


    // Create the local_light matrix.
    create_local_light(local_light, light_rotation);

    // Create the local_screen matrix.
    create_local_screen(local_screen, MATRIXCAST(modelMat), MATRIXCAST(viewMat), MATRIXCAST(mProjMat));

    // Calculate the normal values.
    calculate_normals(temp_normals, vertex_count, normals, local_light);

    // Calculate the lighting values.
    calculate_lights(temp_lights, vertex_count, temp_normals, light_direction, light_colour, light_type, light_count);

    // Calculate the colour values after lighting.
    calculate_colours(temp_colours, vertex_count, colours, temp_lights);

    // Calculate the vertex values.
    calculate_vertices(temp_vertices, vertex_count, vertices, local_screen);

    // Convert floating point vertices to fixed point and translate to center of screen.
    draw_convert_xyz(xyz, 2048, 2048, 32, vertex_count, (vertex_f_t*)temp_vertices);

    // Convert floating point colours to fixed point.
    draw_convert_rgbq(rgbaq, vertex_count, (vertex_f_t*)temp_vertices, (color_f_t*)temp_colours, color->a);

    // Generate the ST register values
    draw_convert_st(st, vertex_count, (vertex_f_t*)temp_vertices, (texel_f_t*)coordinates);

    // Draw the triangles using triangle primitive type.
    //q = draw_prim_start(q,0,prim,color);
    // Use a 64-bit pointer to simplify adding data to the packet
    dw = (u64*)draw_prim_start(q,0,prim,color);

    for(i = 0; i < points_count; i++)
    {
        //q->dw[0] = rgbaq[points[i]].rgbaq;
        //q->dw[1] = xyz[points[i]].xyz;
        //q++;
        *dw++ = rgbaq[points[i]].rgbaq;
        *dw++ = st[points[i]].uv;
        *dw++ = xyz[points[i]].xyz;
    }

    // check if we're in the middle of a qword or not
    if ((u32)dw % 16)
    {
        *dw++ = 0;
    }

    // Only 3 registers rgbaq/st/xyz were used (standard STQ reglist)
    //q = draw_prim_end(q,2,DRAW_RGBAQ_REGLIST);
    q = draw_prim_end((qword_t*)dw, 3, DRAW_STQ_REGLIST);

    // Define our dmatag for the dma chain.
    DMATAG_CNT(dmatag,q-dmatag-1,0,0,0);


    return q;
}

qword_t* Renderer::render_mesh_unindexed(
    Mat4& modelMat,
    Mat4& viewMat,
    VECTOR* vertices,
    VECTOR* normals,
    VECTOR* colours,
    VECTOR* coordinates, // tex coords
    int vertex_count,
    qword_t* q,
    prim_t* prim,
    color_t* color,
    framebuffer_t* frame,
    zbuffer_t* z)
{

    int i;

    qword_t *dmatag;

    MATRIX local_light;
    VECTOR light_rotation = { 0.00f, 0.00f, 0.00f, 1.00f };

    MATRIX local_screen;

    // TODO - not hardcode
    if (((size_t)vertex_count) > sMaxVertCount) {
        printf("Render vertex buffer vertex count > max: %d\n",
            vertex_count);
        return nullptr;
    }

    // Now grab our qword pointer and increment past the dmatag.
    dmatag = q;
    q++;


    // Create the local_light matrix.
    create_local_light(local_light, light_rotation);

    // Create the local_screen matrix.
    create_local_screen(local_screen, MATRIXCAST(modelMat), MATRIXCAST(viewMat), MATRIXCAST(mProjMat));

    // Calculate the normal values.
    if (normals && colours) {
        calculate_normals(temp_normals, vertex_count, normals, local_light);
        
        // Calculate the lighting values.
        calculate_lights(temp_lights, vertex_count, temp_normals, light_direction, light_colour, light_type, light_count);

        // Calculate the colour values after lighting.
        calculate_colours(temp_colours, vertex_count, colours, temp_lights);
    }


    // Calculate the vertex values.
    calculate_vertices(temp_vertices, vertex_count, vertices, local_screen);

    // Convert floating point vertices to fixed point and translate to center of screen.
    draw_convert_xyz(xyz, 2048, 2048, 32, vertex_count, (vertex_f_t*)temp_vertices);

    // Convert floating point colours to fixed point.
    if (normals && colours) {
        draw_convert_rgbq(rgbaq, vertex_count, (vertex_f_t*)temp_vertices, (color_f_t*)temp_colours, color->a);
    }

    // Generate the ST register values
    draw_convert_st(st, vertex_count, (vertex_f_t*)temp_vertices, (texel_f_t*)coordinates);

    // Draw the triangles using triangle primitive type.
    //q = draw_prim_start(q,0,prim,color);
    // Use a 64-bit pointer to simplify adding data to the packet
    dw = (u64*)draw_prim_start(q,0,prim,color);

#if 0
    for(i = 0; i < points_count; i++)
    {
        //q->dw[0] = rgbaq[points[i]].rgbaq;
        //q->dw[1] = xyz[points[i]].xyz;
        //q++;
        *dw++ = rgbaq[points[i]].rgbaq;
        *dw++ = st[points[i]].uv;
        *dw++ = xyz[points[i]].xyz;
    }
#endif
    color_t drawColor;
    drawColor.r = 0x80;
    drawColor.g = 0x80;
    drawColor.b = 0x80;
    drawColor.a = 0x80;
    drawColor.q = 1.0f;
    for (i = 0; i < vertex_count; ++i)
    {
        texel_t st;
        st.u = coordinates[i][0];
        st.v = coordinates[i][1];
        *dw++ = drawColor.rgbaq;
        //*dw++ = st[i].uv;
        *dw++ = st.uv;
        *dw++ = xyz[i].xyz;
    }

    // check if we're in the middle of a qword or not
    if ((u32)dw % 16)
    {
        *dw++ = 0;
    }

    // Only 3 registers rgbaq/st/xyz were used (standard STQ reglist)
    //q = draw_prim_end(q,2,DRAW_RGBAQ_REGLIST);
    q = draw_prim_end((qword_t*)dw, 3, DRAW_STQ_REGLIST);

    // Define our dmatag for the dma chain.
    DMATAG_CNT(dmatag,q-dmatag-1,0,0,0);


    return q;
}

void Renderer::BeginFrame()
{
    framebuffer_t* frame = mFrame;
    zbuffer_t* z = &mZ;

    current = packets[context];
    q = current->data;
    dmatag = q;
    q++;

    // clear framebuffer without any pixel testing
    q = draw_disable_tests(q, 0, z);
    q = draw_clear(q,0,2048.0f-320.0f,2048.0f-256.0f,frame->width,frame->height,0x00,0x00,0x00);
    q = draw_enable_tests(q,0,z);

    DMATAG_CNT(dmatag,q-dmatag - 1,0,0,0);
}

void Renderer::Update()
{
    framebuffer_t* frame = mFrame;
    
#if 1
    dmatag = q;
    q++;
    q = draw_finish(q);
    DMATAG_END(dmatag, q-dmatag-1, 0,0,0);

    // now send our current dma chain
    dma_wait_fast();
    dma_channel_send_chain(DMA_CHANNEL_GIF, current->data, q - current->data, 0,0);
#endif
    //flushPipeline();

    // either block until a vsync, or keep rendering until there's one available
    graph_wait_vsync();

    draw_wait_finish();
    graph_set_framebuffer_filtered(frame[context].address, frame[context].width, frame[context].psm, 0,0);

    // Switch context
    context ^= 1;

    // We need to flip buffers outside of the chain, for some reason,
    // so we use a separate small packet
    flip_buffers(flip_pkt, &frame[context]);

    // Prepare for next frame
    BeginFrame();
}

} // end namespace PS2

