#ifndef MD2_MODEL_H_INCLUDED
#define MD2_MODEL_H_INCLUDED

#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include <Math.h>
#include <Renderer.h>

namespace PS2
{

class Md2Model
{
public:

    struct Animation
    {
        char name[16]; // based on KeyFrame struct name size
        int32_t startFrame;
        int32_t endFrame;
        bool loop;
        
        Animation(int startFrame, int endFrame, bool loop = true) :
            startFrame(startFrame),
            endFrame(endFrame),
            loop(loop)
        {}
    };

    Md2Model();
    ~Md2Model();
    
    bool Load(const char* fileName);
    void Update(const float dt);
    void Render(
        Mat4& modelMat,
        Mat4& viewMat,
        Renderer& render
    );
    
    void SetAnimation(const int start, const int end)
    {
        interpolation = 0.0f;
        startFrame = start;
        endFrame = end;
        nextFrame = startFrame;
        //strcpy(currentAnimName, keyFrames[startFrame].name);
    }
    
    void SetAnimation(const Animation& anim)
    {
        SetAnimation(anim.startFrame, anim.endFrame);
        loopAnim = anim.loop;
    }

    bool SetAnimation(const char* name)
    {
        if (strcmp(name, currentAnimName) == 0) {
            //printf("current anim name already %s, not setting\n",
            //    currentAnimName);
            return true;
        }
        
        for (size_t i = 0; i < animationsSize; ++i) {
            if (strcmp(name, animations[i].name) == 0) {
                strcpy(currentAnimName, name);
                SetAnimation(animations[i]);
                printf("Set anim start, end frame: %ld, %ld\n",
                    animations[i].startFrame, animations[i].endFrame);
                return true;
            }
        }
        return false;
    }
    
    inline char* GetAnimName()
    {
        return currentAnimName;
    }
    inline float GetFrameRadius(size_t frame)
    {
        return radii[frame];
    }

private:

    struct Header
    {
        uint8_t magic[4];       // IDP2, 8
        int32_t version;
        
        int32_t skinWidth;      // tex width, height
        int32_t skinHeight;
        
        int32_t frameSize;      // size of 1 keyframe in bytes
        
        int32_t numSkins;       // number of textures
        int32_t numVertices;
        int32_t numTexCoords;
        int32_t numTriangles;
        int32_t numGLCmds;      // number of opengl draw commands
        int32_t numFrames;      // number of keyframes
        
        int32_t skinOffset;     // offset to skin names (64 bytes each)
        int32_t texCoordOffset; // offset to texture coords
        int32_t triangleOffset; // offset to triangles
        int32_t frameOffset;    // offset to keyframes
        int32_t GLCmdOffset;    // offset to opengl draw commands
        int32_t eofOffset;      // offset to end of file
    };
    
    struct Vertex
    {
        uint8_t v[3];   // compressed vertices; uncompress = scale*v + translate
        uint8_t lightNormIndex;
    };
    
    struct TexCoord
    {
        int16_t s;
        int16_t t;
    };
    
    struct KeyFrame
    {
        float scale[3];     // use to multiply and add Vertex::v
        float translate[3];
        char name[16];
        Vertex* md2Vertices;
        size_t md2VerticesSize;
        Vec4* vertices; // converted result vertices
        size_t verticesSize;
    };
    
    struct Triangle
    {
        int16_t vertIndex[3];
        int16_t texCoordIndex[3];
    };
    
    struct Skin
    {
        char name[64]; // filename of the texture
    };

    Skin* skins;
    size_t skinsSize;
    TexCoord* md2TexCoords;
    size_t md2TexCoordsSize;
    Triangle* triangles;
    size_t trianglesSize;
    
    Vec4* texCoords; // converted tex coords
    size_t texCoordsSize;
    KeyFrame* keyFrames;
    size_t keyFramesSize;
    // TODO  maybe
    //std::vector<std::string> texNames;
    
    KeyFrame interpolatedFrame; // tmp/worker frame
    char currentAnimName[256]; // 256 arbitrarily chosen
    
    int startFrame;
    int endFrame;
    int currentFrame;
    int nextFrame;
    float interpolation;
    bool loopAnim;
    
    VertexBuffer vertexBuf;
    
    float* radii; // radius for each frame
    size_t radiiSize;

    Animation* animations;
    size_t animationsSize;
    
    void reorganizeVertices(); // recreate vertices so that we don't need to use indices
    void stripTextureNames(); // remove folder prefixes from texture names
    void genBuffers(); // create opengl buffers
    void genAnimations();

    inline size_t countNumAnims()
    {
        //printf("Printing anim names\n");
        char curAnimName[16];
        size_t animCount = 1;
        size_t c = 0;
        while (!(keyFrames[0].name[c] >= '0' && keyFrames[0].name[c] <= '9'))
        {
            curAnimName[c] = keyFrames[0].name[c];
            c++;
        }
        curAnimName[c] = '\0';
        c = 0;
        for (size_t i = 1; i < keyFramesSize; ++i)
        {
            char frameAnimName[16];
            while (!(keyFrames[i].name[c] >= '0' && keyFrames[i].name[c] <= '9'))
            {
                frameAnimName[c] = keyFrames[i].name[c];
                c++;
            }
            frameAnimName[c] = '\0';
            c = 0;
            if (strcmp(frameAnimName, curAnimName) != 0) {
                ++animCount;
                strcpy(curAnimName, frameAnimName);
            }
        }

        return animCount;
    }
};

} // namespace PS2

#endif // MD2_MODEL_H_INCLUDED


