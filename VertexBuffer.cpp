#include <VertexBuffer.h>

namespace PS2
{

VertexBuffer::VertexBuffer() :
    mVertices(nullptr),
    mNormals(nullptr),
    mColors(nullptr),
    mTexCoords(nullptr),
    mVertexCount(0),
    mFloatsPerVertex(0),
    mIndices(nullptr),
    mIndicesCount(0)
{}

VertexBuffer::~VertexBuffer()
{}


void VertexBuffer::Init(
    float* vertices,
    float* normals,
    float* colors,
    float* texCoords,
    int vertexCount,
    int* indices,
    int indicesCount)
{
    mVertices = vertices;
    mNormals = normals;
    mColors = colors;
    mTexCoords = texCoords;
    mVertexCount = vertexCount;
    mFloatsPerVertex = 4; // TODO - not hardcode
    mIndices = indices;
    mIndicesCount = indicesCount;
}

}

