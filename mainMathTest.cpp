#include <stdio.h>

#include "RendererPs2sdk.h"
#include "Math.h"

void printMat4(const Mat4& m)
{
    printf(
        "%f,%f,%f,%f\n"
        "%f,%f,%f,%f\n"
        "%f,%f,%f,%f\n"
        "%f,%f,%f,%f\n",
        m.v1d[0], m.v1d[1], m.v1d[2], m.v1d[3],
        m.v1d[4], m.v1d[5], m.v1d[6], m.v1d[7],
        m.v1d[8], m.v1d[9], m.v1d[10], m.v1d[11],
        m.v1d[12], m.v1d[13], m.v1d[14], m.v1d[15]
    );
}

void TestPersp()
{
    Mat4 projMat = Math::Perspective(
        Math::Deg2Rad(45.0f),
        4.0f/3.0f,
        1.0f,
        2000.0f
    );

    printf("projMat:\n");
    printMat4(projMat);
}

int main()
{
    TestPersp();
    SleepThread();
    return 0;
}

