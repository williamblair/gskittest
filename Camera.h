#ifndef PS2_CAMERA_H_INCLUDED
#define PS2_CAMERA_H_INCLUDED

#include "Math.h"
#include "MoveComponent.h"

namespace PS2
{

class Camera
{
public:
    virtual Mat4& GetViewMat() = 0;
};

class FPSCamera : public Camera
{
public:
    FPSCamera();
    ~FPSCamera();

    MoveComponent& GetMoveComponent() { return mMoveComponent; }
    Vec4& GetPosition() { return mMoveComponent.position; }
    Mat4& GetViewMat() { return mViewMat; }

    void Update();
    
private:
    MoveComponent mMoveComponent;
    Mat4 mViewMat;
};

class OrbitCamera : public Camera
{
public:
    
    OrbitCamera();
    
    Mat4& GetViewMat() { return mViewMat; }
    
    void SetMoveComponent(MoveComponent* mc) { mMoveComponent = mc; }
    MoveComponent* GetMoveComponent() { return mMoveComponent; }

    // Added to the target position to look at
    void SetTargetOffset(Vec4 offs) { mTargetOffs = offs; }
    
    void SetDistance(const float d) { mDistance = d; }
    void AddDistance(const float d) { mDistance += d; }
    float GetDistance() const { return mDistance; }
    
    void AddPitch(const float amount) {
        mPitch += amount;
        mPitch = Math::Clamp(mPitch, -80.0f, 80.0f);
    }
    void AddYaw(const float amount) {
        mYaw += amount;
        while (mYaw < 0.0f) { mYaw += 360.0f; }
        while (mYaw > 360.0f) { mYaw -= 360.0f; }
    }
    
    void Update();
    
private:
    Mat4 mViewMat;
    MoveComponent* mMoveComponent; // the position we're following
    Vec4 mPosition;
    Vec4 mTargetOffs;
    float mDistance; // how far away from the moveComponent target
    float mYaw;
    float mPitch;
};

} // end namespace PS2

#endif


