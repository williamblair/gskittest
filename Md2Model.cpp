#include <Md2Model.h>

namespace PS2
{

#if 0
#define DECL_ANIM(name, startFrame, endFrame) \
    const Md2Model::Animation Md2Model::Animation::name = Md2Model::Animation(startFrame, endFrame)
#define DECL_ANIM_NOLOOP(name, startFrame, endFrame) \
    const Md2Model::Animation Md2Model::Animation::name = Md2Model::Animation(startFrame, endFrame, false)

// static member, start frame, end frame    
DECL_ANIM(Idle, 0, 39);
DECL_ANIM(Run, 40, 45);
DECL_ANIM(Attack, 46, 53);
DECL_ANIM(Pain1, 54, 57);
DECL_ANIM(Pain2, 58, 61);
DECL_ANIM(Pain3, 62, 65);
DECL_ANIM(Jump, 66, 71);
DECL_ANIM(FlipOff, 72, 83);
DECL_ANIM(Salute, 84, 94);
DECL_ANIM(Taunt, 95, 111);
DECL_ANIM(Wave, 112, 122);
DECL_ANIM(Point, 123, 134);
DECL_ANIM(CrouchIdle, 135, 153);
DECL_ANIM(CrouchWalk, 154, 159);
DECL_ANIM(CrouchAttack, 160, 168);
DECL_ANIM(CrouchPain, 169, 172);
DECL_ANIM(CrouchDeath, 173, 177);
DECL_ANIM_NOLOOP(Death1, 178, 183);
DECL_ANIM_NOLOOP(Death2, 184, 189);
DECL_ANIM_NOLOOP(Death3, 190, 197);
#endif

#undef DECL_ANIM
#undef DECL_ANIM_NOLOOP

Md2Model::Md2Model() :
    startFrame(0),
    endFrame(0),
    currentFrame(0),
    nextFrame(1),
    interpolation(0.0f),
    loopAnim(false)
{
    currentAnimName[0] = '\0';
}
Md2Model::~Md2Model()
{}
    
bool Md2Model::Load(const char* fileName)
{
    FILE* inFile = fopen(fileName, "r");
    if (!inFile) {
        printf("Failed to open Md2 file: %s\n", fileName);
        return false;
    }
    
    Header header;
    fread((void*)&header, sizeof(Header), 1, inFile);
    
    // Verify file header correctness
    if (header.magic[0] != 'I' ||
        header.magic[1] != 'D' ||
        header.magic[2] != 'P' ||
        header.magic[3] != '2')
    {
        printf("Invalid header: magic != IDP2\n");
        return false;
    }
    
    if (header.version != 8)
    {
        printf("Invalid header: version != 8\n");
        return false;
    }
    
    // reserve space for MD2 data
    skins = (Skin*)malloc(header.numSkins*sizeof(Skin)); skinsSize = header.numSkins;
    texCoords = (Vec4*)memalign(16, header.numTexCoords*sizeof(Vec4)); texCoordsSize = header.numTexCoords;
    md2TexCoords = (TexCoord*)malloc(header.numTexCoords*sizeof(TexCoord)); md2TexCoordsSize = header.numTexCoords;
    triangles = (Triangle*)malloc(header.numTriangles*sizeof(Triangle)); trianglesSize = header.numTriangles;
    keyFrames = (KeyFrame*)malloc(header.numFrames*sizeof(KeyFrame)); keyFramesSize = header.numFrames;
    
    for (size_t i = 0; i < keyFramesSize; ++i)
    {
        keyFrames[i].vertices = (Vec4*)memalign(16, header.numVertices*sizeof(Vec4));
        keyFrames[i].verticesSize = header.numVertices;
        keyFrames[i].md2Vertices = (Vertex*)malloc(header.numVertices*sizeof(Vertex));
        keyFrames[i].md2VerticesSize = header.numVertices;
    }
    
    printf("Keyframes size setvertices: %u\n", keyFramesSize);
    
    // read MD2 components
#define READ_DATA(offset, vec, number, type) \
    fseek(inFile, offset, SEEK_SET); \
    fread((void*)vec, number*sizeof(type), 1, inFile)
    
    READ_DATA(header.skinOffset, skins, header.numSkins, Skin);    
    READ_DATA(header.texCoordOffset, md2TexCoords, header.numTexCoords, TexCoord);
    READ_DATA(header.triangleOffset, triangles, header.numTriangles, Triangle);

#undef READ_DATA
    
    fseek(inFile, header.frameOffset, SEEK_SET);
    for (int i = 0; i < header.numFrames; ++i)
    {
        KeyFrame* f = &keyFrames[i];
        fread((void*)(f->scale), 3*sizeof(float), 1, inFile);
        fread((void*)(f->translate), 3*sizeof(float), 1, inFile);
        fread((void*)(f->name), 16*sizeof(char), 1, inFile);
        fread((void*)(f->md2Vertices), header.numVertices*sizeof(Vertex), 1, inFile);
    }
    
    // scale MD2 vertices into regular OpenGL vertices
    radii = (float*)malloc(keyFramesSize*sizeof(float));
    radiiSize = keyFramesSize;
    size_t radiiIndex = 0;
    for (size_t i = 0; i < keyFramesSize; ++i)
    {
        KeyFrame& frame = keyFrames[i];
        
        float min = 10000.0f;
        float max = -10000.0f;
        
        int k = 0;
        for (size_t j = 0; j < frame.verticesSize; ++j)
        {
            Vec4& vertex = frame.vertices[j];
            
            vertex.x = frame.scale[0] * frame.md2Vertices[k].v[0] + frame.translate[0];
            vertex.z = frame.scale[1] * frame.md2Vertices[k].v[1] + frame.translate[1];
            vertex.y = frame.scale[2] * frame.md2Vertices[k].v[2] + frame.translate[2];
            vertex.w = 1.0f;
             
            ++k;
            
            if (vertex.y < min) min = vertex.y;
            if (vertex.y > max) max = vertex.y;
        }
        
        float frameRadius = (max - min) / 2.0f;
        radii[radiiIndex++] = frameRadius;
    }
        
    // scale tex coords into regular OpenGL tex coords
    int i = 0;
    for (size_t j = 0; j < texCoordsSize; ++j)
    {
        Vec4& texCoord = texCoords[j];
        texCoord.x = float(md2TexCoords[i].s) / float(header.skinWidth);
        texCoord.y = 1.0f - (float(md2TexCoords[i].t) / float(header.skinHeight));
        texCoord.z = 0.0f;
        texCoord.w = 1.0f;
        ++i;
    }
    
    fclose(inFile);
    inFile = nullptr;
    
    reorganizeVertices();
    // TODO/or ignore
    //stripTextureNames();
    
    interpolatedFrame.vertices = (Vec4*)memalign(16, keyFrames[0].verticesSize*sizeof(Vec4));
    interpolatedFrame.verticesSize = keyFrames[0].verticesSize;
    interpolatedFrame.md2Vertices = nullptr;
    interpolatedFrame.md2VerticesSize = 0;
    memcpy(
        (void*)interpolatedFrame.vertices,
        (void*)keyFrames[0].vertices,
        keyFrames[0].verticesSize*sizeof(Vec4)
    );
    strcpy(currentAnimName, keyFrames[0].name);
    printf("Interpolated frame vertices size: %u\n",
        interpolatedFrame.verticesSize);
        
    genBuffers();
    genAnimations();

    return true;
}

void Md2Model::Update(const float dt)
{
    const float FRAMES_PER_SECOND = 8.0f;
    interpolation += dt * FRAMES_PER_SECOND;
    if (interpolation >= 1.0f)
    {
        currentFrame = nextFrame++;
        if (nextFrame > endFrame)
        {
            if (loopAnim)
            {
                nextFrame = startFrame;
            }
            else
            {
                nextFrame = endFrame;
                startFrame = endFrame;
            }

        }
        
        interpolation = 0.0f;
    }
    
    float t = interpolation;
    int i = 0;
    for (size_t j = 0; j < interpolatedFrame.verticesSize; ++j)
    {
        Vec4& vertex = interpolatedFrame.vertices[j];
        
#define LERP(a, b, t) (a) + ((t) * ((b) - (a)))
        
        float x1 = keyFrames[currentFrame].vertices[i].x;
        float x2 = keyFrames[nextFrame].vertices[i].x;
        vertex.x = LERP(x1, x2, t);
        
        float y1 = keyFrames[currentFrame].vertices[i].y;
        float y2 = keyFrames[nextFrame].vertices[i].y;
        vertex.y = LERP(y1, y2, t);
        
        float z1 = keyFrames[currentFrame].vertices[i].z;
        float z2 = keyFrames[nextFrame].vertices[i].z;
        vertex.z = LERP(z1, z2, t);
        
        vertex.w = 1.0f;
        
#undef LERP
        ++i;
    }
    
    // Update buffer data
    vertexBuf.Init(
        (float*)interpolatedFrame.vertices,
        nullptr, // normals
        nullptr, // colors
        (float*)texCoords,
        interpolatedFrame.verticesSize,
        nullptr, // indices
        0 // indicesCount
    );
}

void Md2Model::Render(
    Mat4& modelMat,
    Mat4& viewMat,
    Renderer& render)
{
    render.DrawVertexBuffer(
        modelMat,
        viewMat,
        vertexBuf);
}

// Because some vertices are reused, different texture indices
// by default can refer to the same vertex indices, so we copy these
// vertices so there are no conflictions
void Md2Model::reorganizeVertices()
{
    Vec4* tmpVertices = (Vec4*)memalign(16, trianglesSize*3*sizeof(Vec4));
    Vec4* tmpTexCoords = (Vec4*)memalign(16, trianglesSize*3*sizeof(Vec4));
    size_t tmpVerticesSize = trianglesSize * 3;
    size_t tmpTexCoordsSize = trianglesSize * 3;
    
    bool texCoordsDone = false;
    
    for (size_t frameIndex = 0; frameIndex < keyFramesSize; ++frameIndex)
    {
        KeyFrame& frame = keyFrames[frameIndex];
        
        size_t tmpVertIndex = 0;
        size_t tmpTexIndex = 0;
        
        for (uint32_t i = 0; i < trianglesSize; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                tmpVertices[tmpVertIndex++] = frame.vertices[triangles[i].vertIndex[j]];
                
                // only copy the texture coords once
                if (!texCoordsDone)
                {
                    tmpTexCoords[tmpTexIndex++] = texCoords[triangles[i].texCoordIndex[j]];
                }
            }
        }
        
        // tex coords shared between frames; only copy once
        texCoordsDone = true;
        
        // copy the reassigned vertex data
        if (frame.verticesSize < tmpVerticesSize) {
            free(frame.vertices);
            frame.vertices = (Vec4*)memalign(16, tmpVerticesSize*sizeof(Vec4));
            frame.verticesSize = tmpVerticesSize;
        }
        memcpy(
            (void*)frame.vertices,
            (void*)tmpVertices,
            tmpVerticesSize*sizeof(Vec4)
        );
    }
    
    // copy the reassigned tex coord
    if (texCoordsSize < tmpTexCoordsSize) {
        free(texCoords);
        texCoords = (Vec4*)memalign(16, tmpTexCoordsSize*sizeof(Vec4));
        texCoordsSize = tmpTexCoordsSize;
    }
    memcpy(
        (void*)texCoords,
        (void*)tmpTexCoords,
        tmpTexCoordsSize*sizeof(Vec4)
    );

    free(tmpVertices);
    free(tmpTexCoords);
}

// remove any folder prefixes from the model texture file names
void Md2Model::stripTextureNames()
{
#if 0
    for (Skin& skin : skins)
    {
        std::string texture = skin.name;
        
        size_t fileNameStart = texture.find_last_of("/") + 1;
        size_t lastDot = texture.find_last_of(".");
        
        std::string textureName = texture.substr(fileNameStart, lastDot - fileNameStart);
        texNames.push_back(textureName);
    }
#endif
}

void Md2Model::genBuffers()
{
    vertexBuf.Init(
        (float*)interpolatedFrame.vertices,
        nullptr, // normals
        nullptr, // colors
        (float*)texCoords,
        interpolatedFrame.verticesSize,
        nullptr, // indices
        0 // indicesCount
    );
}

void Md2Model::genAnimations()
{
    animationsSize = countNumAnims();
    animations = (Animation*)malloc(animationsSize*sizeof(Animation));
    char curAnimName[16];
    memset((void*)curAnimName, 0, sizeof(curAnimName));
    size_t animIndex = 0;
    size_t c = 0;
    animations[0].startFrame = 0;
    animations[0].endFrame = 0;
    while (!(keyFrames[0].name[c] >= '0' && keyFrames[0].name[c] <= '9'))
    {
        animations[0].name[c] = keyFrames[0].name[c];
        curAnimName[c] = keyFrames[0].name[c];
        ++c;
    }
    animations[0].name[c] = '\0';
    curAnimName[c] = '\0';
    animations[0].loop = true;
    c = 0;
    for (size_t i = 1; i < keyFramesSize; ++i)
    {
        char frameAnimName[16];
        while (!(keyFrames[i].name[c] >= '0' && keyFrames[i].name[c] <= '9'))
        {
            frameAnimName[c] = keyFrames[i].name[c];
            ++c;
        }
        frameAnimName[c] = '\0';
        c = 0;
        if (strcmp(curAnimName, frameAnimName) != 0) {
            strcpy(curAnimName, frameAnimName);
            animations[animIndex].endFrame = i - 1;
            animIndex++;
            animations[animIndex].startFrame = i;
            animations[animIndex].loop = true;
            strcpy(animations[animIndex].name, curAnimName);
        }
    }
    animations[animationsSize-1].endFrame = keyFramesSize - 1;

    for (size_t i = 0; i < animationsSize; ++i)
    {
        printf("Anim name, start, stop: %s, %ld, %ld\n",
            animations[i].name, 
            animations[i].startFrame, animations[i].endFrame
        );
    }
}

} // namespace PS2

