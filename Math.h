#ifndef MATH_HELPER_H_INCLUDED
#define MATH_HELPER_H_INCLUDED

#include <math.h>
#include <math3d.h>

#define VECTORCAST(vec) \
    (*((VECTOR*)(vec).v))
#define MATRIXCAST(mat) \
    (*((MATRIX*)(mat).v1d))

// Compatible with VECTOR via *((VECTOR*)Vec4::v)
struct Vec4
{
    union
    {
        struct
        {
            float x;
            float y;
            float z;
            float w;
        };
        float v[4];
    };

    Vec4() :
        x(0.0f),
        y(0.0f),
        z(0.0f),
        w(1.0f)
    {}
    
    Vec4(float x, float y, float z, float w) :
        x(x),
        y(y),
        z(z),
        w(w)
    {}

    Vec4(const Vec4& other) {
        vector_copy(VECTORCAST(*this), VECTORCAST(other));
    }

    Vec4& operator=(const Vec4& other) {
        vector_copy(VECTORCAST(*this), VECTORCAST(other));
        return *this;
    }
    
    Vec4& operator+=(const Vec4& other) {
        x += other.x;
        y += other.y;
        z += other.z;
        w += other.w;
        return *this;
    }
    Vec4& operator-=(const Vec4& other) {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        w -= other.w;
        return *this;
    }
} __attribute__((aligned(16)));

inline Vec4 operator*(const Vec4& lhs, const Vec4& rhs) {
    Vec4 result;
    vector_multiply(VECTORCAST(result), VECTORCAST(lhs), VECTORCAST(rhs));
    return result;
}
inline Vec4 operator*(const Vec4& lhs, const float rhs) {
    return Vec4(
        lhs.x * rhs,
        lhs.y * rhs,
        lhs.z * rhs,
        lhs.w * rhs
    );
}
inline Vec4 operator*(const float lhs, const Vec4&  rhs) {
    return Vec4(
        lhs * rhs.x,
        lhs * rhs.y,
        lhs * rhs.z,
        lhs * rhs.w
    );
}
inline Vec4 operator+(const Vec4& lhs, const Vec4& rhs) {
    Vec4 result(
        lhs.x + rhs.x,
        lhs.y + rhs.y,
        lhs.z + rhs.z,
        lhs.w + rhs.w
    );
    return result;
}
inline Vec4 operator-(const Vec4& lhs, const Vec4& rhs) {
    Vec4 result(
        lhs.x - rhs.x,
        lhs.y - rhs.y,
        lhs.z - rhs.z,
        lhs.w - rhs.w
    );
    return result;
}

// Compatible with MATRIX via Mat4::v1d
struct Mat4
{
    union
    {
        struct
        {
            float row0[4];
            float row1[4];
            float row2[4];
            float row3[4];
        };
        float v1d[16];
        float v2d[4][4];
    };

    Mat4() { matrix_unit(MATRIXCAST(*this)); }
    Mat4(const Mat4& other) {
        matrix_copy(MATRIXCAST(*this), MATRIXCAST(other));
    }
    Mat4& operator=(const Mat4& other) {
        matrix_copy(MATRIXCAST(*this), MATRIXCAST(other));
        return *this;
    }
} __attribute__((aligned(16)));

inline Mat4 operator*(const Mat4& lhs, const Mat4& rhs) {
    Mat4 result;
    matrix_multiply(
        MATRIXCAST(result), MATRIXCAST(lhs), MATRIXCAST(rhs));
    return result;
}
inline Vec4 operator*(const Mat4& lhs, const Vec4& rhs) {
    // TODO - optimize
    return Vec4(
        rhs.x*lhs.row0[0] + rhs.y*lhs.row1[0] + rhs.z*lhs.row2[0] + rhs.w*lhs.row3[0],
        rhs.x*lhs.row0[1] + rhs.y*lhs.row1[1] + rhs.z*lhs.row2[1] + rhs.w*lhs.row3[1],
        rhs.x*lhs.row0[2] + rhs.y*lhs.row1[2] + rhs.z*lhs.row2[2] + rhs.w*lhs.row3[2],
        rhs.x*lhs.row0[3] + rhs.y*lhs.row1[3] + rhs.z*lhs.row2[3] + rhs.w*lhs.row3[3]
    );
}

namespace Math
{
    static const float Pi = M_PI;
    static const float TwoPi = 2.0f*M_PI;
    inline float Rad2Deg(const float rad) { return rad * 180.0f / M_PI; }
    inline float Deg2Rad(const float deg) { return deg * M_PI / 180.0f; }
    inline float Clamp(const float val, const float min, const float max) {
        return (val < min ? min :
                (val > max ? max : val));
    }

    /*inline Vec4 Normalize(Vec4& vec) {
        Vec4 result;
        vector_normalize(VECTORCAST(result), VECTORCAST(vec));
        return result;
    }*/
    inline Vec4 Normalize(Vec4 vec) {
        Vec4 result;
        vector_normalize(VECTORCAST(result), VECTORCAST(vec));
        return result;
    }
    inline Vec4 Cross(Vec4 a, Vec4 b) {
        Vec4 result(
            a.y*b.z - a.z*b.y,
            a.z*b.x - a.x*b.z,
            a.x*b.y - a.y*b.x,
            1.0f
        );
        return result;
    }
    inline float Dot(Vec4& a, Vec4& b) {
        return vector_innerproduct(VECTORCAST(a), VECTORCAST(b));
    }

    inline Mat4 Transpose(Mat4& m) {
        Mat4 result;
        matrix_transpose(MATRIXCAST(result), MATRIXCAST(m));
        return result;
    }
    inline Mat4 Translate(float x, float y, float z) {
        Mat4 result;
        // copied from math3d.c in ps2sdk
        result.v1d[0x0C] = x;
        result.v1d[0x0D] = y;
        result.v1d[0x0E] = z;
        return result;
    }
    // x, y, and z axis rotation in radians
    inline Mat4 Rotate(float x, float y, float z) {
        Mat4 result;
        Mat4 tmp; // defaults to identity
        Vec4 vec(x,y,z,1.0f);
        matrix_rotate(
            MATRIXCAST(result),
            MATRIXCAST(tmp),
            VECTORCAST(vec)
        );
        return result;
    }
    // rotation around an arbitrary axis a
    inline Mat4 Rotate(const float angRad, Vec4 a) {
        const float c = cosf(angRad);
        const float s = sinf(angRad);
        Mat4 res;
        float* p = res.v1d;

        p[0] = c + (1 - c) * a.x*a.x;
        p[1] = (1 - c) * a.x*a.y - s*a.z;
        p[2] = (1 - c) * a.x*a.z + s*a.y;
        p[3] = 0.0f;

        p[4] = (1 - c) * a.x*a.y + s*a.z;
        p[5] = c + (1 - c) * a.y*a.y;
        p[6] = (1 - c) * a.y*a.z - s*a.x;
        p[7] = 0.0f;

        p[8] = (1 - c) * a.x*a.z - s*a.y;
        p[9] = (1 - c) * a.y*a.z + s*a.x;
        p[10] = c + (1 - c) * a.z*a.z;
        p[11] = 0.0f;

        p[12] = 0.0f;
        p[13] = 0.0f;
        p[14] = 0.0f;
        p[15] = 1.0f;

        return Transpose(res);
    }
    inline Mat4 Scale(float x, float y, float z) {
        Mat4 result;
        Mat4 tmp; // defaults to identity
        Vec4 vec(x,y,z,1.0f);
        matrix_scale(
            MATRIXCAST(result),
            MATRIXCAST(tmp),
            VECTORCAST(vec)
        );
        return result;
    }
    // https://www.geertarien.com/blog/2017/07/30/breakdown-of-the-lookAt-function-in-OpenGL/
    inline Mat4 LookAt(Vec4& eye, Vec4& target, Vec4& up) {
        Vec4 zaxis = Normalize(target - eye);
        Vec4 xaxis = Normalize(Cross(zaxis, up));
        Vec4 yaxis = Cross(xaxis, zaxis);
        
        zaxis.x = -zaxis.x;
        zaxis.y = -zaxis.y;
        zaxis.z = -zaxis.z;
        zaxis.w = 1.0f;
        
        xaxis.w = -Dot(xaxis, eye);
        yaxis.w = -Dot(yaxis, eye);
        zaxis.w = -Dot(zaxis, eye);
        
        Mat4 res;
        vector_copy(*((VECTOR*)res.row0), VECTORCAST(xaxis));
        vector_copy(*((VECTOR*)res.row1), VECTORCAST(yaxis));
        vector_copy(*((VECTOR*)res.row2), VECTORCAST(zaxis));
        // res.row3 = 0,0,0,1 by default
        res = Transpose(res);
        return res;
    }
    inline Mat4 LookAtBjTest(const Vec4& eye, const Vec4& target, const Vec4& up)
    {
        Vec4 cameraDirection = Normalize(eye - target);
        Vec4 cameraRight = Normalize(Cross(up, cameraDirection));
        Vec4 cameraUp = Normalize(Cross(cameraDirection, cameraRight));

        //printf("Camera Direction, right, up:\n");
        //printVec4(cameraDirection);
        //printVec4(cameraRight);
        //printVec4(cameraUp);
        
        Mat4 basisMat;
        {
            // column major order
            //float* p = glm::value_ptr(basisMat);
            basisMat.v2d[0][0] = cameraRight.x; basisMat.v2d[1][0] = cameraUp.x; basisMat.v2d[2][0] = cameraDirection.x; basisMat.v2d[3][0] = 0.0f;
            basisMat.v2d[0][1] = cameraRight.y; basisMat.v2d[1][1] = cameraUp.y; basisMat.v2d[2][1] = cameraDirection.y; basisMat.v2d[3][1] = 0.0f;
            basisMat.v2d[0][2] = cameraRight.z; basisMat.v2d[1][2] = cameraUp.z; basisMat.v2d[2][2] = cameraDirection.z; basisMat.v2d[3][2] = 0.0f;
            basisMat.v2d[0][3] = 0.0f; basisMat.v2d[1][3] = 0.0f; basisMat.v2d[2][3] = 0.0f; basisMat.v2d[3][3] = 1.0f;
            basisMat = Transpose(basisMat);
            //printf("basisMat:\n");
            //printMat4(basisMat);
        }
        Vec4 negEye = -1.0f*eye;
        //printf("neg eye:\n");
        //printVec4(negEye);
        Mat4 trans = Translate(negEye.x, negEye.y, negEye.z);
        //printf("trans:\n");
        //printMat4(trans);
        //Mat4 view = basisMat * trans;
        Mat4 view = trans * basisMat;
        //printf("view:\n");
        //printMat4(view);
        return view;
    }
    // http://learnwebgl.brown37.net/08_projections/projections_perspective.html
    inline Mat4 Perspective(
        const float fovy, // field of view angle in radians
        const float aspect, // aspect ratio
        const float near,
        const float far)
    {
        // create_view_screen does the aspect adjustment
        const float top = near * tanf(fovy/2.0f);
        const float bottom = -top;
        const float right = top/* * aspect*/;
        const float left = -right;
        
        // modified version of create_view_screen
        Mat4 result;
        /*matrix_unit(MATRIXCAST(result));
        result.v1d[0x00] = (2 * near) / (right - left);
        result.v1d[0x05] = (2 * near) / (top - bottom);
        result.v1d[0x08] = (right + left) / (right - left);
        result.v1d[0x09] = (top + bottom) / (top - bottom);
        result.v1d[0x0A] = -((far + near) / (far - near));
        result.v1d[0x0B] = -1.00f;
        result.v1d[0x0E] = -((2 * far * near) / (far - near));
        result.v1d[0x0F] = 0.00f;*/
        create_view_screen(
            MATRIXCAST(result),
            aspect,
            left, right,
            bottom, top,
            near, far
        );
        return result;
    }
}

#endif

