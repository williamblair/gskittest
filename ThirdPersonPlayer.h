#ifndef THIRD_PERSON_PLAYER_H_INCLUDED
#define THIRD_PERSON_PLAYER_H_INCLUDED

#include <stdio.h>

#include <Renderer.h>
#include <Camera.h>
#include <Texture.h>
#include <Md2Model.h>


namespace PS2
{

class ThirdPersonPlayer
{
public:
    ThirdPersonPlayer();
    ~ThirdPersonPlayer();

    void SetModel(Md2Model* model) {
        mModel = model;
        Vec4 targetOffs(0.0f, 0.0f, 0.0f, 1.0f);
        targetOffs.y = model->GetFrameRadius(0);
        mCamera.SetTargetOffset(targetOffs);
    }
    // Used when drawing the model, without affecting movement calculation
    void SetDrawYawOffset(const float yawDegrees) { mDrawYawOffs = yawDegrees; }
    Md2Model* GetModel() { return mModel; }
    
    void SetTexture(Texture* tex) { mTexture = tex; }
    Texture* GetTexture() { return mTexture; }

    Mat4& GetViewMat() { return mCamera.GetViewMat(); }
    Mat4& GetModelMat() { return mModelMat; }

    OrbitCamera& GetCamera() { return mCamera; }

    Vec4& GetPosition() { return mPosition; }

    void Update(const float dt) {
        if (mModel == nullptr) {
            printf("ThirdPersonPlayer update: mModel null\n");
            return;
        }
        mModel->Update(dt);
        mCamera.Update();
    }

    // Move forward based on x/y values of analog stick
    void Move(const float x, const float y, const float amount);

    void Draw(Renderer& render)
    {
        if (mModel == nullptr ||
            mTexture == nullptr)
        {
            printf("ThirdPersonPlayer Draw: mModel/mTexture null\n");
            return;
        }
        render.SetTexture(*mTexture);
        mModel->Render(mModelMat, GetViewMat(), render);
    }

private:
    Md2Model* mModel;
    Texture* mTexture;
    Mat4 mModelMat;
    Vec4 mPosition;
    MoveComponent mMoveComp;
    OrbitCamera mCamera;
    float mDrawYawOffs;
};

} // namespace PS2

#endif // THIRD_PERSON_PLAYER_H_INCLUDED

