#ifndef MOVE_COMPONENT_H_INCLUDED
#define MOVE_COMPONENT_H_INCLUDED

#include "Math.h"

struct MoveComponent
{
    Vec4 position;
    Vec4 target;
    Vec4 up;
    Vec4 forward;
    Vec4 right;
    
    float pitch;
    float yaw;
    
    MoveComponent();
    
    void Update();
    
    void MoveForward(const float amount) {
        position += forward * amount;
        position.w = 1.0f;
    }
    void MoveRight(const float amount) {
        position += right * amount;
        position.w = 1.0f;
    }
    
    void AddPitch(const float amount) {
        pitch += amount;
        pitch = Math::Clamp(pitch, -80.0f, 80.0f);
    }
    void AddYaw(const float amount) {
        yaw += amount;
        while (yaw < 0.0f) { yaw += 360.0f; }
        while (yaw > 360.0f) { yaw -= 360.0f; }
    }
};

#endif // MOVE_COMPONENT_H_INCLUDED


