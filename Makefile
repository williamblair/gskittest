# _____     ___ ____     ___ ____
#  ____|   |    ____|   |        | |____|
# |     ___|   |____ ___|    ____| |    \    PS2DEV Open Source Project.
#-----------------------------------------------------------------------
# Copyright 2001-2004, ps2dev - http://www.ps2dev.org
# Licenced under Academic Free License version 2.0
# Review ps2sdk README & LICENSE files for further details.

EE_BIN = gsKitTest.elf
EE_OBJS = main.o \
          Renderer.o \
          Camera.o \
          MoveComponent.o \
          Controller.o \
          VertexBuffer.o \
          Texture.o \
          Md2Model.o \
          ThirdPersonPlayer.o \
          TileFloor.o
#EE_OBJS = mainMathTest.o
EE_LIBS := -lmath3d -ldraw -lpad -lgraph -lpacket -ldma -lpatches
EE_INCS := -I$(GSKIT)/include -I.
EE_LDFLAGS = -L$(GSKIT)/lib
EE_CXXFLAGS := -DASSETS_DIR=\"host:\"
#EE_CXXFLAGS := -DASSETS_DIR=\"cdfs:\"

all: $(EE_BIN)
	$(EE_STRIP) --strip-all $(EE_BIN)

clean:
	rm -f $(EE_BIN) $(EE_OBJS)

run: $(EE_BIN)
	ps2client execee host:$(EE_BIN)

reset:
	ps2client reset

iso:
	rm -rf cdrom
	mkdir cdrom
	cp SYSTEM.CNF cdrom
	cp $(EE_BIN) cdrom/MAIN.ELF
	cp -r data cdrom
	mkisofs -o gskittest.iso cdrom

include $(PS2SDK)/samples/Makefile.pref
include $(PS2SDK)/samples/Makefile.eeglobal_cpp

