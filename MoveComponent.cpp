#include "MoveComponent.h"
#include <stdio.h>

MoveComponent::MoveComponent() :
    position(0.0f, 0.0f, 0.0f, 1.0f),
    target(0.0f, 0.0f, -1.0f, 1.0f),
    up(0.0f, 1.0f, 0.0f, 1.0f),
    forward(0.0f, 0.0f, -1.0f, 1.0f),
    right(1.0f, 0.0f, 0.0f, 1.0f),
    pitch(0.0f),
    yaw(0.0f)
{}

void MoveComponent::Update()
{
    // calculate right axis
    //right = Math::angleAxis(Math::ToRadians(yaw), Vec3(0.0f, 1.0f, 0.0f)) * // yaw about y axis
    //    Vec3(1.0f, 0.0f, 0.0f);
    //right = Math::Rotate(0.0f, Math::Deg2Rad(yaw), 0.0f) * Vec4(1.0f, 0.0f, 0.0f, 1.0f);
    right = Math::Rotate(Math::Deg2Rad(yaw), Vec4(0.0f, 1.0f, 0.0f, 1.0f)) *
        Vec4(1.0f, 0.0f, 0.0f, 1.0f);
    right = Math::Normalize(right);
    right.w = 1.0f;
    
    // calculate up axis
    up = Math::Rotate(Math::Deg2Rad(pitch), right) * // pitch about rotated x axis
        //Vec3(0.0f, 1.0f, 0.0f);
        Vec4(0.0f, 1.0f, 0.0f, 1.0f);
    up = Math::Normalize(up);
    up.w = 1.0f;
    
    // calculate forward axis
    forward = Math::Cross(up, right);
    forward = Math::Normalize(forward);
    forward.w = 1.0f;

    //printf("Position: %f, %f, %f, %f\n", position.x, position.y, position.z, position.w);
    //printf("Forward: %f, %f, %f, %f\n", forward.x, forward.y, forward.z, forward.w);
    //printf("target: %f, %f, %f, %f\n", target.x, target.y, target.z, target.w);
    //printf("up: %f, %f, %f, %f\n", up.x, up.y, up.z, up.w);
    
    // set target equal to a bit forward from the eye position
    target = position + forward;
    //target.y = 10.0f;
    //target = Vec4(0.0f, 10.0f, 0.0f, 1.0f);
    target.w = 1.0f;
}


