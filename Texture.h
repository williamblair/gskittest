#ifndef PS2_TEXTURE_H_INCLUDED
#define PS2_TEXTURE_H_INCLUDED

#include <kernel.h>
#include <stdlib.h>
#include <tamtypes.h>
#include <math3d.h>

#include <packet.h>

#include <dma_tags.h>
#include <gif_tags.h>
#include <gs_psm.h>

#include <dma.h>

#include <graph.h>

#include <draw.h>
#include <draw3d.h>

namespace PS2
{

class Texture
{

friend class Renderer;

public:
    Texture();
    ~Texture();

    // Load from raw data
    bool Init(const char* data, const int numChannels, int width, int height);

    // Load from a TARGA image
    bool LoadFromTarga(const char* fileName);
    
private:
    char* mData;
    int mWidth;
    int mHeight;
    int mNumChannels;

    static texbuffer_t sTexbuf;
    static packet_t* sLoadTexPkt;
    static packet_t* sSetupTexPkt;
    static const int sMaxTexWidth;
    static const int sMaxTexHeight;

    bool load_texture(
        const char* data,
        const int numChannels,
        const int width, const int height,
        texbuffer_t* texbuf,
        qword_t** q
    );
    void setup_texture(
        const int width,
        const int height,
        texbuffer_t* texbuf,
        qword_t** q
    );
};

} // namespace PS2

#endif // PS2_TEXTURE_H_INCLUDED

