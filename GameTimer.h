#ifndef GAME_TIME_H_INCLUDED
#define GAME_TIME_H_INCLUDED

// PS2DEV SDK:
#include <kernel.h>
#include <tamtypes.h>
#include <draw.h>
#include <graph.h>
//#include <gs_psm.h>


#include <time.h>
#include <unistd.h>

namespace PS2
{

static inline u32 millisecondsSinceStartup()
{
    return clock() / (CLOCKS_PER_SEC / 1000);
}

static inline float msecToSec(const float ms)
{
    return ms * 0.001f;
}

static inline float secToMsec(const float sec)
{
    return sec * 1000.0f;
}

static inline u32 secToMsec(const u32 sec)
{
    return sec * 1000;
}

// get the elapsed time in milliseconds since the last call to this function.
// NOT thread safe
/*static u32 clockMilliseconds()
{
    static u32 baseTime = 0;
    static u32 currentTime = 0;
    static bool timerInitialized = false;
    
    if (!timerInitialized)
    {
        baseTime = millisecondsSinceStartup();
        timerInitialized = true;
    }
    
    currentTime = millisecondsSinceStartup() - baseTime;
    return currentTime;
}*/

class GameTimer
{
public:
    GameTimer() :
        lastTimeMs(0)
    {
    }
    
    // returns dela time in seconds
    float Update()
    {
        u32 curTimeMs = millisecondsSinceStartup();
        u32 deltaMs = curTimeMs - lastTimeMs;
        float dt = msecToSec((float)deltaMs);
        lastTimeMs = curTimeMs;
        return dt;
    }    

private:
    u32 lastTimeMs;
};

} // end namespace PS2

#endif // GAME_TIME_H_INCLUDED


