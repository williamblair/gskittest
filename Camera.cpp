#include "Camera.h"

namespace PS2
{

FPSCamera::FPSCamera()
{
    /*mMoveComponent.position = Vec4(0.0f, 10.0f, 80.0f, 1.0f);
    mMoveComponent.target = Vec4(0.0f, 10.0f, 0.0f, 1.0f);
    mMoveComponent.up = Vec4(0.0f, 1.0f, 0.0f, 1.0f);*/
    mViewMat = Math::LookAtBjTest(
        mMoveComponent.position,
        mMoveComponent.target,
        mMoveComponent.up
    );
}

FPSCamera::~FPSCamera()
{
}

void FPSCamera::Update()
{
    mMoveComponent.Update();

    // Update view matrix
    mViewMat = Math::LookAtBjTest(
        mMoveComponent.position,
        mMoveComponent.target,
        mMoveComponent.up
    );
}

OrbitCamera::OrbitCamera() :
    mMoveComponent(nullptr),
    mPosition(Vec4(200.0f, 0.0f, 0.0f, 1.0f)),
    mTargetOffs(Vec4(0.0f, 0.0f, 0.0f, 1.0f)),
    mDistance(200.0f),
    mYaw(0.0f),
    mPitch(0.0f)
{
    Update();
}

void OrbitCamera::Update()
{
    Vec4 targetPos(0.0f, 0.0f, 0.0f, 1.0f);
    if (mMoveComponent) {
        targetPos = mMoveComponent->position;
    }
    targetPos = targetPos + mTargetOffs;
    
    // calculate right axis
    //Vec3 right = Math::angleAxis(Math::ToRadians(mYaw), Vec3(0.0f, 1.0f, 0.0f)) * // yaw about y axis
    //    Vec3(1.0f, 0.0f, 0.0f);
    Vec4 right = Math::Rotate(Math::Deg2Rad(mYaw), Vec4(0.0f, 1.0f, 0.0f, 1.0f)) *
        Vec4(1.0f, 0.0f, 0.0f, 1.0f);
    right = Math::Normalize(right);
    right.w = 1.0f;
    
    // calculate up axis
    //Vec4 up = Math::angleAxis(Math::ToRadians(mPitch), right) * // pitch about rotated x axis
    //    Vec3(0.0f, 1.0f, 0.0f);
    Vec4 up = Math::Rotate(Math::Deg2Rad(mPitch), right) *
        Vec4(0.0f, 1.0f, 0.0f, 1.0f);
    up = Math::Normalize(up);
    up.w = 1.0f;
    
    // calculate forward axis
    Vec4 forward = Math::Cross(up, right);
    forward = Math::Normalize(forward);
    forward.w = 1.0f;
    
    // set our location backwards from the target
    mPosition = targetPos - forward * mDistance;
    
    // update view matrix
    //mViewMat = Math::lookAt(mPosition, targetPos, Vec3(0.0f, 1.0f, 0.0f));
    mViewMat = Math::LookAtBjTest(mPosition, targetPos, Vec4(0.0f, 1.0f, 0.0f, 1.0f));
}

} // end namespace PS2


