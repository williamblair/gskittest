#ifndef TILE_FLOOR_H_INCLUDED
#define TILE_FLOOR_H_INCLUDED

#include <Texture.h>
#include <VertexBuffer.h>
#include <Renderer.h>

namespace PS2
{

class TileFloor
{
public:
    TileFloor();
    ~TileFloor();

    void SetTexture(Texture* tex) { mTexture = tex; }
    void SetPosition(Vec4 pos) { mPosition = pos; }
    void SetScale(float scale) { mScale = Vec4(scale, scale, scale, 1.0f); }

    void Draw(Vec4 playerPos, Mat4& viewMat, Renderer& render);

private:
    static VertexBuffer sVertBuf;
    static Vec4 sVertices[6];
    static Vec4 sTexCoords[6];
    Vec4 mPosition; // top left coords
    Vec4 mScale;
    Texture* mTexture;
};

}

#endif // TILE_FLOOR_H_INCLUDED

