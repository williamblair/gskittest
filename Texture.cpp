#include <Texture.h>
#include <stdio.h>
#include <string.h>

namespace PS2
{

texbuffer_t Texture::sTexbuf;
packet_t* Texture::sLoadTexPkt = nullptr;
packet_t* Texture::sSetupTexPkt = nullptr;
const int Texture::sMaxTexWidth = 256;
const int Texture::sMaxTexHeight = 256;

Texture::Texture() :
    mData(nullptr)
{}

Texture::~Texture()
{}

bool Texture::Init(const char* data, const int numChannels, int width, int height)
{
    // initial memory has not been allocated
    if (sLoadTexPkt == nullptr) {
        sLoadTexPkt = packet_init(50, PACKET_NORMAL);
        sSetupTexPkt = packet_init(10, PACKET_NORMAL);
        sTexbuf.width = width;
        sTexbuf.psm = GS_PSM_24;
        sTexbuf.address = graph_vram_allocate(
            sMaxTexWidth, sMaxTexHeight,
            GS_PSM_24,
            GRAPH_ALIGN_BLOCK
        );
        if (!sTexbuf.address) {
            printf("graph_vram_allocate failed\n");
            return false;
        }
    }
    if (width > sMaxTexWidth ||
        height > sMaxTexHeight)
    {
        printf("Image too large: %d, %d\n", width, height);
        return false;
    }
    mData = (char*)data;
    mWidth = width;
    mHeight = height;
    mNumChannels = numChannels;

    if (numChannels != 3) {
        printf("Unsupported num channels: %d\n", numChannels);
        return false;
    }
    qword_t* q = sLoadTexPkt->data;
    load_texture(
        //data,
        mData,
        numChannels,
        width, height,
        &/*mTexbuf*/sTexbuf,
        &q
    );
    q = sSetupTexPkt->data;
    setup_texture(width, height, &/*mTexbuf*/sTexbuf, &q);

    return true;
}

bool Texture::load_texture(
    const char* data,
    const int numChannels,
    const int width,
    const int height,
    texbuffer_t* texbuf,
    qword_t** q)
{
    //packet_t* packet = packet_init(50, PACKET_NORMAL);
    //qword_t* q = packet->data;
    //qword_t* q = sLoadTexPkt->data;
    //q = packet->data;
    //q = sLoadTexPkt->data;
    //qword_t* qOrig = *q;

    if (numChannels != 3) {
        printf("Unsupported num channels: %d\n", numChannels);
        return false;
    }
    *q = draw_texture_transfer(
        *q,
        (void*)data,
        width, height,
        GS_PSM_24,
        texbuf->address,
        texbuf->width
    );
    *q = draw_texture_flush(*q);

    dma_channel_send_chain(DMA_CHANNEL_GIF, sLoadTexPkt->data, (*q) - sLoadTexPkt->data, 0,0);
    dma_wait_fast();

    //packet_free(packet);

    return true;
}

void Texture::setup_texture(
    const int width,
    const int height,
    texbuffer_t* texbuf,
    qword_t** q)
{
    //packet_t* packet = packet_init(10, PACKET_NORMAL);
    //qword_t* q = packet->data;
    //qword_t* q = sSetupTexPkt->data;
    //qword_t* qOrig = *q;

    // Using a texture involves setting up a lot of information
    clutbuffer_t clut;
    lod_t lod;

    lod.calculation = LOD_USE_K;
    lod.max_level = 0;
    lod.mag_filter = LOD_MAG_NEAREST;
    lod.min_filter = LOD_MIN_NEAREST;
    lod.l = 0;
    lod.k = 0;

    // assumes width/height are powers of 2
    texbuf->info.width = draw_log2(width);
    texbuf->info.height = draw_log2(height);
    // TODO - support other num channels
    texbuf->info.components = TEXTURE_COMPONENTS_RGB;
    texbuf->info.function = TEXTURE_FUNCTION_DECAL;

    // No CLUTs for RGB24 data
    clut.storage_mode = CLUT_STORAGE_MODE1;
    clut.start = 0;
    clut.psm = 0;
    clut.load_method = CLUT_NO_LOAD;
    clut.address = 0;

    *q = draw_texture_sampling(*q, 0, &lod);
    *q = draw_texturebuffer(*q, 0, texbuf, &clut);

    // Now send the packet, no need to wait since it's the first
    dma_channel_send_normal(DMA_CHANNEL_GIF, /*packet*/sSetupTexPkt->data, (*q) - /*packet*/sSetupTexPkt->data, 0, 0);
    dma_wait_fast();

    //packet_free(packet);
}

namespace Targa
{
// imageDesc bitmasks,
// bit 4 is left-to-right ordering,
// bit 5 is top-to-bottom ordering
enum IMAGE_ORIENTATIONS
{
    BOTTOM_LEFT  = 0x00,    // first px is bottom left
    BOTTOM_RIGHT = 0x10,    // first px is bottom right
    TOP_LEFT     = 0x20,    // first px is top left
    TOP_RIGHT    = 0x30     // first px is top right
};

enum FILE_TYPES
{
    TFT_NO_DATA = 0,
    TFT_INDEXED = 1,
    TFT_RGB = 2,
    TFT_GRAYSCALE = 3,
    TFT_RLE_INDEXED = 9,
    TFT_RLE_RGB = 10,
    TFT_RLE_GRAYSCALE = 11
};

struct Header
{
    uint8_t idLength;
    uint8_t colorMapType;
    uint8_t imageTypeCode;
    uint8_t colorMapSpec[5];
    uint16_t xOrigin;
    uint16_t yOrigin;
    uint16_t width;
    uint16_t height;
    uint8_t bpp;        // bits per pixel
    uint8_t imageDesc;
}; 

bool IsImageTypeSupported(Header& header)
{
    // only support color images currently
    return ((header.imageTypeCode == TFT_RGB ||
            header.imageTypeCode == TFT_RLE_RGB) &&
            header.colorMapType == 0);
}

bool IsCompressed(const Header& header)
{
    return (header.imageTypeCode == TFT_RLE_RGB ||
            header.imageTypeCode == TFT_RLE_GRAYSCALE);
}

bool IsUncompressed(const Header& header)
{
    return (header.imageTypeCode == TFT_RGB ||
            header.imageTypeCode == TFT_GRAYSCALE);
}

bool LoadCompressed(
    FILE* inFile,
    Header& header,
    unsigned int width,
    unsigned int height,
    unsigned int bitsPerPixel,
    unsigned int bytesPerPixel,
    uint8_t* imageData)
{
    unsigned int pixelCount = height * width;
    unsigned int curPixel = 0;
    unsigned int curByte = 0;

    // assumes max bytesPerPixel == 4
    //std::vector<uint8_t> colorBuffer(bytesPerPixel);
    uint8_t colorBuffer[4];

    do
    {
        uint8_t chunkHeader = 0;
        //inFile.read((char*)&chunkHeader, sizeof(uint8_t)); // read in a single byte
        fread((void*)&chunkHeader, sizeof(uint8_t), 1, inFile); // read in a single byte

        if (chunkHeader < 128)
        {
            chunkHeader++;

            for (short counter = 0; counter < chunkHeader; counter++)
            {
                // read a color chunk
                //inFile.read((char*)colorBuffer.data(), bytesPerPixel);
                fread((void*)colorBuffer, bytesPerPixel, 1, inFile);

                imageData[curByte+0] = colorBuffer[2];
                imageData[curByte+1] = colorBuffer[1];
                imageData[curByte+2] = colorBuffer[0];

                if (bytesPerPixel == 4) {
                    imageData[curByte + 3] = colorBuffer[3];
                }

                curByte += bytesPerPixel;
                curPixel++;

                // mismatch between sizes
                if (curPixel > pixelCount) {
                    return false;
                }
            }
        }
        // chunkHeader >= 128
        else
        {
            chunkHeader -= 127;

            // read a color chunk
            //inFile.read((char*)colorBuffer.data(), bytesPerPixel);
            fread((void*)colorBuffer, bytesPerPixel, 1, inFile);

            for (short counter = 0; counter < chunkHeader; counter++)
            {
                imageData[curByte+0] = colorBuffer[2];
                imageData[curByte+1] = colorBuffer[1];
                imageData[curByte+2] = colorBuffer[0];
                
                if (bytesPerPixel == 4) {
                    imageData[curByte + 3] = colorBuffer[3];
                }

                curByte += bytesPerPixel;
                curPixel++;

                // size mismatch
                if (curPixel > pixelCount) {
                    return false;
                }
            }
        }

    } while (curPixel < pixelCount);

    //free(colorBuffer);
    return true;
}

bool LoadUncompressed(
    FILE* inFile,
    Header& header,
    unsigned int width,
    unsigned int height,
    unsigned int bitsPerPixel,
    unsigned int bytesPerPixel,
    uint8_t* imageData)
{
    //unsigned int imageSize = imageData.size();
    //inFile.read((char*)imageData.data(), imageSize);
    unsigned int imageSize = width * height * bytesPerPixel;
    fread((void*)imageData, imageSize * sizeof(uint8_t), 1, inFile);

    // swap R and B components (BGR --> RGB)
    for (unsigned int swap = 0; swap < imageSize; swap += bytesPerPixel)
    {
        uint8_t cswap = imageData[swap];
        imageData[swap] = imageData[swap+2];
        imageData[swap+2] = cswap;
    }

    return true;
}

void FlipImageVertically(
    FILE* inFile,
    Header& header,
    unsigned int width,
    unsigned int height,
    unsigned int bitsPerPixel,
    unsigned int bytesPerPixel,
    uint8_t* imageData)
{
    //std::vector<uint8_t> flippedData(imageData.size());
    uint8_t* flippedData = (uint8_t*)malloc(width*height*bytesPerPixel*sizeof(uint8_t));
    size_t flippedIndex = 0;
    int step = bytesPerPixel;
    for (int row = height - 1; row >= 0; --row) {
        for (int col = 0; col < (int)(width * step); ++col) {
            uint8_t* rowData = &imageData[row * width * step];

            for (unsigned int i = 0; i < width * step; ++i) {
                flippedData[flippedIndex] = *rowData;
                flippedIndex++;
                rowData++;
            } 
        }
    }

    //imageData.assign(flippedData.begin(), flippedData.end());
    memcpy(
        (void*)imageData,
        (void*)flippedData,
        width*height*bytesPerPixel*sizeof(uint8_t));
    free(flippedData);
}

} // end namespace Targa

bool Texture::LoadFromTarga(const char* fileName)
{
    Targa::Header header;
    unsigned int width;
    unsigned int height;
    unsigned int bitsPerPixel;
    unsigned int bytesPerPixel;
    uint8_t* imageData;

    //std::ifstream inFile(fileName, std::ios::binary);
    //if (!inFile.is_open()) {
    //    std::cerr << __func__ << " failed to open targa image file: " 
    //              << fileName << std::endl;
    //    return false;
    //}
    FILE* inFile = fopen(fileName, "r");
    if (!inFile) {
        printf("Failed to open targa image file: %s\n", fileName);
        return false;
    }

    //inFile.read((char*)&header, sizeof(Header));
    fread((char*)&header, sizeof(header), 1, inFile);
    if (!Targa::IsImageTypeSupported(header)) {
        //std::cerr << __func__ << " unsupported targa type" << std::endl;
        printf("Unsupported targa type for file: %s\n", fileName);
        fclose(inFile);
        return false;
    }

    width = header.width;
    height = header.height;

    bitsPerPixel = header.bpp;
    bytesPerPixel = header.bpp / 8;

    if (bytesPerPixel < 3) {
        //std::cerr << __func__ << " unsupported bytesPerPixel: " 
        //          << bytesPerPixel << std::endl;
        printf("Unsupported bytesPerPixel: %u\n", bytesPerPixel);
        return false;
    }

    unsigned int imageSize = width * height * bytesPerPixel;
    //imageData.resize(imageSize);
    imageData = (uint8_t*)malloc(imageSize*sizeof(uint8_t));
    if (!imageData) {
        printf("Failed to alloc image data for tga: %s\n", fileName);
        fclose(inFile);
        return false;
    }

    // skip past the id if there is one
    if (header.idLength > 0) {
        //inFile.ignore(header.idLength);
        fseek(inFile, header.idLength, SEEK_CUR);
    }

    bool result = false;

    if (Targa::IsUncompressed(header)) {
        result = Targa::LoadUncompressed(
            inFile,
            header,
            width, height,
            bitsPerPixel, bytesPerPixel,
            imageData);
    } else {
        result = Targa::LoadCompressed(
            inFile,
            header,
            width, height,
            bitsPerPixel, bytesPerPixel,
            imageData);
    }

    if ((header.imageDesc & Targa::TOP_LEFT) == Targa::TOP_LEFT) {
        Targa::FlipImageVertically(
            inFile,
            header,
            width, height,
            bitsPerPixel, bytesPerPixel,
            imageData);
    }

    fclose(inFile);
    if (!result) { return false; }

    result = Init((const char*)imageData, bytesPerPixel, width, height);

    return result;
}

} // end namespace PS2

