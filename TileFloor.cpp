#include <TileFloor.h>
#include <stdio.h>

namespace PS2
{

Vec4 TileFloor::sVertices[6] = {
    { -1.0f, 0.0f, 1.0f, 1.0f }, // bottom left
    {  1.0f, 0.0f, 1.0f, 1.0f }, // bottom right
    {  1.0f, 0.0f,-1.0f, 1.0f }, // top right
    
    { -1.0f, 0.0f, 1.0f, 1.0f }, // bottom left
    {  1.0f, 0.0f,-1.0f, 1.0f }, // top right
    { -1.0f, 0.0f,-1.0f, 1.0f }  // top left
};

Vec4 TileFloor::sTexCoords[6] = {
    { 0.0f, 0.0f, 0.0f, 0.0f }, // bottom left
    { 1.0f, 0.0f, 0.0f, 0.0f }, // bottom right
    { 1.0f, 1.0f, 0.0f, 0.0f }, // top right

    { 0.0f, 0.0f, 0.0f, 0.0f }, // bottom left
    { 1.0f, 1.0f, 0.0f, 0.0f }, // top right
    { 0.0f, 1.0f, 0.0f, 0.0f }  // top left
};

VertexBuffer TileFloor::sVertBuf;

TileFloor::TileFloor() :
    mPosition(0.0f, 0.0f, 0.0f, 1.0f),
    mScale(1.0f, 1.0f, 1.0f, 1.0f),
    mTexture(nullptr)
{
    sVertBuf.Init(
        (float*)sVertices,
        nullptr,
        nullptr,
        (float*)sTexCoords,
        6,
        nullptr,
        0
    );
}

TileFloor::~TileFloor()
{
}

void TileFloor::Draw(
    Vec4 playerPos,
    Mat4& viewMat,
    Renderer& render)
{
    if (mTexture == nullptr) {
        printf("TileFloor draw tex is null\n");
        return;
    }
    // assumes all scale components are equal
    const float tileSize = 2.0f * mScale.x;
    float x = mPosition.x;
    float z = mPosition.z;
    const size_t numTilesX = 32;
    const size_t numTilesZ = 32;
    Mat4 modelMat;
    
    render.SetTexture(*mTexture);
    for (size_t i = 0; i < numTilesX; ++i)
    {
        z = mPosition.z;
        for (size_t j = 0; j <numTilesZ; ++j)
        {
            Vec4 tilePos(x, mPosition.y, z, 1.0f);
            Vec4 playerDiff = playerPos - tilePos;

            // don't try to draw if a certain distance away...
            if (playerDiff.x*playerDiff.x +
                playerDiff.y*playerDiff.y +
                playerDiff.z*playerDiff.z <
                200.0f*200.0f)
            {
                // currently SRT, TODO make TRS
                modelMat = Math::Scale(mScale.x, 1.0f, mScale.z) *
                    Math::Translate(x, mPosition.y, z);
                render.DrawVertexBuffer(modelMat, viewMat, sVertBuf);
            }
            
            z += tileSize;
        }
        x += tileSize;
    }
}

} // namespace PS2

