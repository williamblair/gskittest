#ifndef PS2_VERTEX_BUFFER_H_INCLUDED
#define PS2_VERTEX_BUFFER_H_INCLUDED

namespace PS2
{

class VertexBuffer
{

friend class Renderer;

public:
    VertexBuffer();
    ~VertexBuffer();

    void Init(
        float* vertices,
        float* normals,
        float* colors,
        float* texCoords,
        int vertexCount,
        int* indices,
        int indicesCount
    );

private:
    float* mVertices;
    float* mNormals;
    float* mColors;
    float* mTexCoords; // VECTOR (4 floats, last 2 unused)
    int mVertexCount;
    int mFloatsPerVertex;
    int* mIndices;
    int mIndicesCount;
};

} // end namespace PS2

#endif

