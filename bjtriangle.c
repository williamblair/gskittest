int points_count = 3;
int points[3] = {
    0, 1, 2
};

int vertex_count = 3;
VECTOR vertices[3] = {
    { -1.0f, -1.0f, 0.0f, 1.0f },
    {  1.0f, -1.0f, 0.0f, 1.0f },
    {  0.0f,  1.0f, 0.0f, 1.0f }
};

VECTOR normals[3] = {
    { 0.0f, 0.0f, 1.0f, 1.0f },
    { 0.0f, 0.0f, 1.0f, 1.0f },
    { 0.0f, 0.0f, 1.0f, 1.0f }
};

VECTOR colours[3] = {
    { 1.0f, 0.0f, 0.0f, 1.0f },
    { 0.0f, 1.0f, 0.0f, 1.0f },
    { 0.0f, 0.0f, 1.0f, 1.0f },
};

