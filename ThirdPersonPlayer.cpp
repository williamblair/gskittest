#include <ThirdPersonPlayer.h>

namespace PS2
{

ThirdPersonPlayer::ThirdPersonPlayer() :
    mModel(nullptr),
    mTexture(nullptr),
    mDrawYawOffs(0.0f)
{
    mCamera.SetMoveComponent(&mMoveComp);
    mCamera.SetDistance(200.0f);
    mPosition = Vec4(0.0f, 0.0f, 0.0f, 1.0f);
}

ThirdPersonPlayer::~ThirdPersonPlayer()
{}


void ThirdPersonPlayer::Move(
    const float x,
    const float y,
    const float amount)
{
    // Put the model animation to idle
    if (fabsf(x) < 0.300f && fabsf(y) < 0.300f) {
        mModel->SetAnimation("idle");
        return;
    }
    
    const float xyLen = sqrtf(x*x + y*y);
    const float normX = x / xyLen;
    const float normY = y / xyLen;
    mMoveComp.yaw = Math::Rad2Deg(atan2f(normY, normX));
    while (mMoveComp.yaw >= 360.0f) { mMoveComp.yaw -= 360.0f; }
    while (mMoveComp.yaw <= 0.0f) { mMoveComp.yaw += 360.0f; }
    mMoveComp.pitch = 0.0f; // no pitch allowed
    mMoveComp.Update();
    mMoveComp.MoveForward(amount);
    // TODO - this multiplication seems backwards to what I want...
    // (expecting TRS form, not SRT)
    mModelMat =
        Math::Rotate(0.0f, Math::Deg2Rad(mMoveComp.yaw + mDrawYawOffs), 0.0f) *
        Math::Translate(
            mMoveComp.position.x,
            mMoveComp.position.y,
            mMoveComp.position.z
        );

    // TODO - not necessary...
    mCamera.SetMoveComponent(&mMoveComp);

    // Put the model animation to move; will do nothing if already set to move
    mModel->SetAnimation("run");

    // save position
    mPosition = mMoveComp.position;
}

} // namespace PS2

