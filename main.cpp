#include <Renderer.h>
#include <Camera.h>
#include <Controller.h>
#include <GameTimer.h>
#include <VertexBuffer.h>
#include <Texture.h>
#include <Md2Model.h>
#include <ThirdPersonPlayer.h>
#include <TileFloor.h>

// cube data
namespace cube
{
//#include "mesh_data.c"
#include "cube_data.c"
static PS2::VertexBuffer vertexBuffer;
static Mat4 modelMat1;
static Mat4 modelMat2;
Vec4 position(0.00f, 0.00f, 0.00f, 1.00f);
Vec4 rotation(0.00f, 0.00f, 0.00f, 1.00f);
static inline void Init()
{
    vertexBuffer.Init(
        (float*)vertices,
        (float*)normals,
        (float*)colours,
        (float*)coordinates,
        vertex_count,
        points,
        points_count
    );
    modelMat1 = Math::Translate(position.x, position.y, position.z);
    modelMat2 = Math::Translate(position.x - 30.0f, position.y, position.z);
}
} // namespace cube

// flower texture data
namespace flowerTexture
{
#include "flower.c"
static PS2::Texture texture;
static inline bool Init()
{
    const int numChannels = 3;
    const int width = 256, height = 256;
    return texture.Init(
        (const char*)flower,
        numChannels,
        width, height
    );
}
}

static PS2::Renderer gRender;
static PS2::GameTimer gTimer;
//static PS2::FPSCamera gCamera;
static PS2::Controller gPad;
static PS2::Md2Model gMd2;
static PS2::ThirdPersonPlayer gPlayer;
static PS2::TileFloor gFloor;

#if 0
static void MoveCamera(const float dt)
{
    const float moveSpeed = 30.0f;
    if (gPad.GetLeftJoyX() < 50) {
        gCamera.GetMoveComponent().MoveRight(-moveSpeed * dt);
    }
    else if (gPad.GetLeftJoyX() > 200) {
        gCamera.GetMoveComponent().MoveRight(moveSpeed * dt);
    }

    if (gPad.GetLeftJoyY() < 50) {
        gCamera.GetMoveComponent().MoveForward(moveSpeed * dt);
    }
    else if (gPad.GetLeftJoyY() > 200) {
        gCamera.GetMoveComponent().MoveForward(-moveSpeed * dt);
    }

    const float rotSpeed = 15.0f;
    if (gPad.GetRightJoyX() < 50) {
        gCamera.GetMoveComponent().AddYaw(rotSpeed * dt);
    }
    else if (gPad.GetRightJoyX() > 200) {
        gCamera.GetMoveComponent().AddYaw(-rotSpeed * dt);
    }
    if (gPad.GetRightJoyY() < 50) {
        gCamera.GetMoveComponent().AddPitch(rotSpeed * dt);
    }
    else if (gPad.GetRightJoyY() > 200) {
        gCamera.GetMoveComponent().AddPitch(-rotSpeed * dt);
    }
}
#endif

static void MovePlayer(const float dt)
{
    int x = gPad.GetLeftJoyX();
    int y = gPad.GetLeftJoyY();
    float xNorm = ((float)(x - 127)) / 127.0f;
    float yNorm = ((float)(y - 127)) / 127.0f;
    const float moveSpeed = 30.0f;
    gPlayer.Move(xNorm, yNorm, moveSpeed * dt * 0.25f);
}

static void RotateCamera(const float dt)
{
    PS2::OrbitCamera& oc = gPlayer.GetCamera();
    const float rotSpeed = 8.0f;
    if (gPad.GetRightJoyX() < 50) {
        oc.AddYaw(rotSpeed * dt);
    }
    else if (gPad.GetRightJoyX() > 200) {
        oc.AddYaw(-rotSpeed * dt);
    }
    if (gPad.GetRightJoyY() < 50) {
        oc.AddPitch(rotSpeed * dt);
    }
    else if (gPad.GetRightJoyY() > 200) {
        oc.AddPitch(-rotSpeed * dt);
    }
}

static bool TestFile()
{
    FILE* fp = fopen(ASSETS_DIR"data/grass.tga", "r");
    if (!fp) {
        printf("Failed to open grass.tga\n");
        return false;
    }
    fseek(fp, 0, SEEK_END);
    size_t fsize = ftell(fp);
    printf("grass.tga file size: %u\n", fsize);
    fclose(fp);
    return true;
}

int main()
{
    PS2::Texture testTarga;
    PS2::Texture md2Texture;
    PS2::Texture floorTexture;
    
    if (!gRender.Init()) { return 1; }
    if (!gPad.Init(0)) { return 1; }
    //if (!TestFile()) { return 1; }
    if (!gMd2.Load(ASSETS_DIR"data/Archvile/Archvile.md2")) { return 1; }
    if (!md2Texture.LoadFromTarga(ASSETS_DIR"data/Archvile/archvile.tga")) { return 1; }
    //if (!gMd2.Load("cdfs:data/Ogro/tris.md2")) { return 1; }
    //if (!md2Texture.LoadFromTarga("cdfs:data/Ogro/Ogrobase.tga")) { return 1; }
    if (!floorTexture.LoadFromTarga(ASSETS_DIR"data/grass.tga")) { return 1; }

    //cube::Init();
    //if (!flowerTexture::Init()) { return 1; }
    //if (!testTarga.LoadFromTarga("cdfs:data/grass.tga")) { return 1; }

    //gCamera.GetMoveComponent().position = Vec4(0.0f, 30.0f, 400.0f, 1.0f);
    //gCamera.GetMoveComponent().target = Vec4(0.0f, 30.0f, 399.0f, 1.0f);

    //Mat4 md2ModelMat = Math::Translate(0.0f, 0.0f, 0.0f);
    //gMd2.SetAnimation(PS2::Md2Model::Animation::Run);
    //if (!gMd2.SetAnimation("run")) {
    //if (!gMd2.SetAnimation("stand")) {
    //    printf("Failed to set idle animation\n");
    //    return 1;
    //}
    //gRender.SetTexture(md2Texture);

    gMd2.SetAnimation("idle");
    gPlayer.SetModel(&gMd2);
    gPlayer.SetTexture(&md2Texture);
    gPlayer.GetCamera().SetDistance(800.0f);
    gPlayer.SetDrawYawOffset(90.0f);

    {
        Vec4 floorPos(0.0f, 0.0f, 0.0f, 1.0f);
        gFloor.SetTexture(&floorTexture);
        gFloor.SetPosition(floorPos);
        gFloor.SetScale(50.f);
    }

    //gCamera.Update();
    gPad.Update();
    gPlayer.Update(0.0f);

    for (;;)
    {
        const float dt = gTimer.Update();
        
        //MoveCamera(dt);
        //gMd2.Update(dt*0.05f);

        /*gRender.SetTexture(testTarga);
        gRender.DrawVertexBuffer(
            cube::modelMat1,
            gCamera.GetViewMat(),
            cube::vertexBuffer
        );

        gRender.SetTexture(flowerTexture::texture);
        gRender.DrawVertexBuffer(
            cube::modelMat2,
            gCamera.GetViewMat(),
            cube::vertexBuffer
        );*/

        //gRender.SetTexture(md2Texture);
        //gMd2.Render(
        //    md2ModelMat,
        //    gCamera.GetViewMat(),
        //    gRender
        //);

        gFloor.Draw(gPlayer.GetPosition(), gPlayer.GetCamera().GetViewMat(), gRender);

        gPlayer.Draw(gRender);

        MovePlayer(dt);
        RotateCamera(dt);
        gPlayer.Update(dt * 0.25f);

        //gCamera.Update();
        gPad.Update();
        gRender.Update();
    }

    SleepThread();
    return 0;
}

